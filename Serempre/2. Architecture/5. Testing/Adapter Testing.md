# Adapter Testing

When working with the [[4. Infrastructure#In-Memory Adapter|TodoInMemAdapter]] and [[4. Infrastructure#API Adapter|TodoApiAdapter]], thorough testing ensures that their behavior aligns with expectations and guarantees robustness in handling different scenarios. This section outlines sample test cases to validate the functionality of these adapters.

## In-Memory Adapter Tests

```ts title="todo-inmem.adapter.test.ts"
// src/features/todo/infrastructure/todo-inmem.adapter.test.ts

import { TodoInMemAdapter } from "./todo-inmem.adapter";

describe("TodoInMemAdapter", () => {
  let adapter: TodoInMemAdapter;

  beforeEach(() => {
    adapter = new TodoInMemAdapter();
  });

  it("should return todos", async () => {
    const todos = await adapter.getTodos();
    expect(todos.length).toBeGreaterThan(0);
  });

  it("should get todo by id", async () => {
    const todoId = "1";
    const todo = await adapter.getTodoById(todoId);
    expect(todo?.id).toBe(todoId);
  });

  // Add more tests for other methods and scenarios
});
```

## API Adapter

```ts title="todo-api.adapter.test.ts"
// src/features/todo/infrastructure/todo-api.adapter.test.ts

import { TodoApiAdapter } from "./todo-api.adapter";

describe("TodoApiAdapter", () => {
  const baseUrl = "http://example.com/api"; // Update with your API URL
  let adapter: TodoApiAdapter;

  beforeEach(() => {
    adapter = new TodoApiAdapter({ baseUrl });
  });

  it("should return todos from the API", async () => {
    const todos = await adapter.getTodos();
    expect(todos.length).toBeGreaterThan(0);
  });

  it("should get todo by id from the API", async () => {
    const todoId = "1";
    const todo = await adapter.getTodoById(todoId);
    expect(todo?.id).toBe(todoId);
  });

  // Add more tests for other methods and scenarios
});
```
