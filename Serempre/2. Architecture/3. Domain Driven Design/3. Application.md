# Application
Contains the use cases or application-specific business logic. It serves as an intermediary between the Presentation and Domain layers. Here’s how you can structure and implement this layer:
- Define separate use case modules for different application functionalities.
- Use plain JavaScript functions or classes to implement the use cases.
- Keep the use cases independent of the Presentation layer and external dependencies.
## Structure:
- **Organize by Functionality:** Divide your application's functionalities into separate use case modules. Each module should contain the relevant use cases related to that functionality.
- **Independent of Presentation:** Keep the use cases decoupled from the Presentation layer. This separation ensures that your business logic remains reusable and adaptable across different user interfaces.
- **Minimize External Dependencies:** Aim to minimize external dependencies in this layer. Instead, focus on using the abstractions provided by the Domain and Infrastructure layers.
## Implementation:
- **Use Functions or Classes:** Implement each use case using plain JavaScript functions or classes, depending on the complexity of the functionality. Functions work well for simpler use cases, while classes offer better organization for more complex scenarios.
- **Dependency Injection:** Follow dependency injection principles to provide necessary dependencies to your use cases. This makes testing and swapping out implementations easier.

```ts
// src/features/todo/application/todo.use-case.ts

import type { TodoAdapter } from '../domain/todo.adapter';
import type { TodoRepository } from '../domain/todo.repository';

class TodoUseCase implements TodoRepository { // Also known as implementation
	private todoAdapter: TodoAdapter;

	constructor(_todoAdapter: TodoAdapter) {
		this.todoAdapter = _todoAdapter;
	}
	
	// Define application-specific use cases
	getTodos = () => {
		return this.todoAdapter.getTodos();
	}
	
	// Retrieve a specific todo by ID
	getTodoById = (todoId: string) => {
		return this.todoAdapter.getTodoById(todoId);
	}
}

export { TodoUseCase }
```