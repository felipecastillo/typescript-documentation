## Axios
HttpService provides an abstraction for making HTTP requests using the [Axios](https://axios-http.com) library.

```ts
/**
 * It simplifies communication with APIs by offering methods for common HTTP verbs (GET, POST, PUT, DELETE).
 *
 * @module HttpService
 */

import axios, { AxiosInstance } from 'axios';

class HttpService {
  private instance: AxiosInstance;

  /**
   * Creates an instance of HttpService with a given base URL.
   *
   * @constructor
   * @param {string} baseURL - The base URL for API requests.
   */
  constructor(baseURL: string, customConfig?: AxiosRequestConfig) {
    this.instance = axios.create({
      baseURL,
      ...customConfig, // Merge user-provided configuration
    });
  }

  /**
   * Performs a GET request to the specified URL.
   *
   * @param {string} url - The URL to send the GET request to.
   * @returns {Promise<any>} The response data.
   */
  async get<T>(url: string): Promise<T> {
    const response = await this.instance.get<T>(url);
    return response.data;
  }

  /**
   * Performs a POST request to the specified URL with data.
   *
   * @param {string} url - The URL to send the POST request to.
   * @param {any} data - The data to be sent in the request body.
   * @returns {Promise<any>} The response data.
   */
  async post<T>(url: string, data: any): Promise<T> {
    const response = await this.instance.post<T>(url, data);
    return response.data;
  }

  /**
   * Performs a PUT request to the specified URL with data.
   *
   * @param {string} url - The URL to send the PUT request to.
   * @param {any} data - The data to be sent in the request body.
   * @returns {Promise<any>} The response data.
   */
  async put<T>(url: string, data: any): Promise<T> {
    const response = await this.instance.put<T>(url, data);
    return response.data;
  }

  /**
   * Performs a DELETE request to the specified URL.
   *
   * @param {string} url - The URL to send the DELETE request to.
   */
  async delete(url: string): Promise<void> {
    await this.instance.delete(url);
  }
}

export default HttpService;
```

## Fetch API
HttpService provides an abstraction for making HTTP requests using different HTTP libraries.
```ts
/**
 * It simplifies communication with APIs by offering methods for common HTTP verbs (GET, POST, PUT, DELETE).
 * This module allows developers to switch between different HTTP libraries or implementations easily.
 *
 * @module HttpService
 */

/**
 * The HttpService class provides an abstraction for making HTTP requests.
 * It allows developers to interact with APIs using common HTTP verbs.
 */
class HttpService {
  private baseUrl: string;

  /**
   * Creates an instance of HttpService with a given base URL.
   *
   * @constructor
   * @param {string} baseURL - The base URL for API requests.
   */
  constructor(baseURL: string) {
    this.baseUrl = baseURL;
  }

  /**
   * Performs an HTTP GET request using the window.fetch API.
   *
   * @param {string} url - The URL to send the GET request to.
   * @returns {Promise<any>} The response data.
   */
  async get<T>(url: string): Promise<T> {
    const response = await fetch(`${this.baseUrl}${url}`);
    if (!response.ok) {
      throw new Error(`HTTP GET request failed with status ${response.status}`);
    }
    return response.json();
  }

  // Rest of the methods
}

export default HttpService;

```