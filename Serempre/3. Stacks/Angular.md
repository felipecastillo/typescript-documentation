# Angular

Angular is a comprehensive and opinionated framework for building web applications. In this example, we'll demonstrate how to use Angular with TypeScript to fetch and display a list of products.

### Example with Angular's HttpClient (Recommended)
```ts
import { Component, OnInit } from '@angular/core';
import { Products, ProductsApiAdapter, type Product } from 'acme';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  isLoading = true;

  constructor() { }

  ngOnInit(): void {
    // Create an instance of Products and initialize it with a ProductsApiAdapter
    const productsService = new Products(new ProductsApiAdapter());

    // Fetch the list of products using Angular's HttpClient
    productsService.getAll()
      .subscribe(
        (data) => {
          this.products = data;
          this.isLoading = false;
        },
        (error) => {
          console.error('Error fetching products:', error);
          this.isLoading = false;
        }
      );
  }
}

```