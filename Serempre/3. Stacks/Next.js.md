# Next.js

Next.js is a popular React framework that simplifies server-side rendering, routing, and state management. It's a great choice for building dynamic, SEO-friendly web applications. In this section, we'll provide two examples of how to use Next.js with TypeScript to fetch and display a list of products.

---
## Features

- New `/app` dir,
- Routing, Layouts, Nested Layouts and Layout Groups
- Data Fetching, Caching and Mutation
- Loading UI
- Route handlers
- Metadata files
- Server and Client Components
- API Routes and Middlewares
- Styled using **Tailwind CSS**
- Validations using **Zod**
- Written in **TypeScript**

---
## Roadmap

- [ ]  ~~Add MDX support for basic pages~~
- [ ]  ~~Build marketing pages~~
- [ ]  ~~Subscriptions using Stripe~~
- [ ] Documentation and blog using **MDX** and **Contentlayer**
- [ ] Subscriptions using **Stripe**
- [ ] UI Components built using **shadcn**
- [ ] Authentication using **NextAuth.js**
- [x]  ~~Responsive styles~~
- [x]  Dark mode

---
## Examples
### Example with `react-query` (Recommended)

```tsx
import { useQuery } from "@tanstack/react-query";

import { Products, ProductsApiAdapter } from "acme";

export default function Page() {
	// Create an instance of Products and initialize it with a ProductsApiAdapter
	const { getAll } = new Products(new ProductsApiAdapter());

	 // Use the useQuery hook to fetch the list of products
	const docs = useQuery({
		queryKey: ["products"],
		queryFn: getAll,
		keepPreviousData: true,
	});
	
	if (isLoading) return <div>Loading...</div>;

	return (
		<section>
			{products.map((product) => (
				<div key={product.id}>
					<h3>{product.name}</h3>
					<p>{product.description}</p>
				</div>
			))}
		</section>
	);
}
```

In this Next.js example, we import and use the `useQuery` hook from `@tanstack/react-query` to fetch data asynchronously. We also create an instance of the `Products` class and initialize it with a `ProductsApiAdapter` to interact with product data.

The `useQuery` hook allows us to fetch the list of products using the `getAll` function and display them in the component once the data is loaded.

### Example without `useQuery`

```tsx
import { useEffect, useState } from "react";

import { Products, ProductsApiAdapter } from "acme";

export default function Page() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Create an instance of Products and initialize it with a ProductsApiAdapter
    const productsService = new Products(new ProductsApiAdapter());

    // Fetch the list of products
    productsService
      .getAll()
      .then((data) => {
        setProducts(data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching products:", error);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) return <div>Loading...</div>;

  return (
    <section>
      {products.map((product) => (
        <div key={product.id}>
          <h3>{product.name}</h3>
          <p>{product.description}</p>
        </div>
      ))}
    </section>
  );
}

```

While both examples demonstrate how to fetch and display data in a Next.js application, it's recommended to use `react-query` for data fetching. `react-query` simplifies data management, provides caching, and offers a better developer experience for handling asynchronous operations in your application. It's a powerful tool for building robust and efficient web applications.
