## Motivation
### Why Use Hexagonal Architecture?

Hexagonal Architecture, also known as Ports and Adapters or Clean Architecture, offers several benefits that can make your software development process more efficient and maintainable:

1. **Modularity**: Hexagonal Architecture promotes the separation of concerns. The domain logic, use cases, and infrastructure details are clearly separated into different layers, making it easier to understand and modify individual components without affecting the whole system.
    
2. **Testability**: By isolating the domain logic from external dependencies using ports and adapters, it becomes straightforward to write unit tests for your core business logic. You can mock or replace adapters during testing to ensure reliable and repeatable tests.
    
3. **Flexibility**: Hexagonal Architecture makes it easier to adapt to changing requirements. You can switch out adapters (e.g., replace a database with an API) or update the user interface without affecting the core functionality of your application.
    
4. **Maintainability**: The clear separation of concerns and the use of interfaces (ports) make your codebase more maintainable. Developers can work on different parts of the system in parallel, and updates or changes to one part are less likely to introduce bugs in other areas.
    
5. **Scalability**: As your application grows, Hexagonal Architecture provides a solid foundation for scaling. You can add new use cases, replace or upgrade infrastructure components, and extend the application without compromising its stability.
    
6. **Documentation and Understanding**: The architectural pattern itself serves as documentation for your project. It makes it easier for new developers to understand the structure and flow of the application.

In summary, Hexagonal Architecture helps you build robust, testable, and adaptable software systems that can evolve with your project's changing needs while maintaining a clear separation of concerns.

---
## Table Of Contents:
### ⚙️ Getting Started
1. [[1. Getting Started/1. Introduction|Introduction]]
2. [[2. Quick Start|Quick Start]]
3. [[3. About|About]]
4. [[4. FAQ|FAQ]]
### 🗄️ Architecture
1. [[2. Architecture/1. Introduction|Introduction]]
2. [[2. Featured-Driven Structure|Featured-Driven Structure]]
3. [[2. Architecture/3. Domain Driven Design/1. Introduction|Domain Driven Design]]
4. [[2. Architecture/1. Introduction#Testing|Testing]]
5. [[2. Architecture/1. Introduction#Best Practices|Best Practices]]
### 👁️ StackB
- [[Next.js]] - WIP
- [[Angular]] - WIP
- More documentation coming soon